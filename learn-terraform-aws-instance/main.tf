terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_instance" "app_server" {
//  ami           = "ami-0742b4e673072066f" // AWS LINUX 2 AMI Sparx us-east-1
  ami           = "ami-096fda3c22c1c990a" // RHEL 8 Sparx us-east-1
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    "sg-001965da509231598", // security group id for Sparx us-east-1
  ]
  subnet_id = "subnet-0a5428b0215c25db5" // subnet id for Sparx us-east-1
  tags = {
    Name = var.instance_name
  }
}